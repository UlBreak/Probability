public class Cout {
    public static void main(String[] args) {
        CriticalDamage CoutCrit = new CriticalDamage();
        Blocking CoutBlock = new Blocking();
        PitProbability CoutProbability = new PitProbability();
        CoutProbability.striking();
        CoutBlock.blockABlow();
    }
}
/*  Пример вывода в консоли
Вероятность попадание
	> 95.0 % против 28.9%
	> Вы, попали во врага
Критический урон
	> 25.3 % против 17.8%
	>Крит!
Вероятность блока
	> 55.5 % против 52.5%
	>Вы, заблокировали удар
*/