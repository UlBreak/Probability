import java.util.Random;

public class PitProbability {
    public void striking() {
        float PIT = 95.0f;  //константа, являющаяся вероятностью попадания во врага
        System.out.println("Вероятность попадание ");
        boolean Probability = EstimationProbability(PIT);


        if (Probability) {
            System.out.println("\t>" + " Вы, попали во врага"); // "\t>" создаёт "   >" в поле вывода
            CriticalDamage.striking(); //при попадание смотрим выпадает крит или нет
        }
        else {
            System.out.println("\t> Вы не попали");
        }
    }

    private static final int MAX = 1_001; // 100%/максимально возможная вероятность

    public boolean EstimationProbability(float pit) {
        Random Rnd = new Random();
        int CriticalChance = (int) Math.min(Math.max((pit * 10), 0), MAX - 1); //вероятность попадания в цель
        int RandChance = Rnd.nextInt(MAX); //случайная вероятность попадания во врага
        System.out.println("\t> " + (CriticalChance / 10.f) + " % против " + (RandChance / 10.f) + "%");
        return CriticalChance > RandChance; //сравниваем константу и выпавший процент и возвращаем true||false
    }
}
