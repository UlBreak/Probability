//Смотрите коментарии в классе PitProbability, всё аналогично

import java.util.Random;

public class CriticalDamage {

    public static void striking() {
        float CHANCE = 25.3f;

        System.out.println("Критический урон ");
        boolean CriticalDamage = EstimationCritical(CHANCE);

        double Damage = 100;

        if (CriticalDamage) {
            Damage *= 2.5;
            System.out.println("\t>" + "Крит!");
        }
        else {
            System.out.println("\t> Вы нанесли " + Damage + "по цели.");
        }
    }

    private static final int MAX = 1_001;

    public static boolean EstimationCritical(float Chance) {
        Random Rnd = new Random();
        int CriticalChance = (int) Math.min(Math.max((Chance * 10), 0), MAX - 1);
        int RandDamage = Rnd.nextInt(MAX);
        System.out.println("\t> " + (CriticalChance / 10.f) + " % против " + (RandDamage / 10.f) + "%");
        return CriticalChance > RandDamage;
    }
}
