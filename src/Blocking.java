//Смотрите коментарии в классе PitProbability, всё аналогично

import java.util.Random;

public class Blocking {
    public void blockABlow() {
        float CHANCE = 55.5f;

        System.out.println("Вероятность блока");
        boolean ChanceBlock = estimationBlock(CHANCE);

        if (ChanceBlock) {
            System.out.println("\t>" + "Вы, заблокировали удар");
        }
        else {
            System.out.println("\t> Вы не заблокировали удар");
        }
    }

    private static final int MAX = 1_001;

    public boolean estimationBlock(float Chance) {
        Random Rnd = new Random();
        int BlockChance = (int) Math.min(Math.max((Chance * 10), 0), MAX - 1);
        int RandBlock = Rnd.nextInt(MAX);
        System.out.println("\t> " + (BlockChance / 10.f) + " % против " + (RandBlock / 10.f) + "%");
        return BlockChance > RandBlock;
    }
}


